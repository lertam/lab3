namespace Lab2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.inputBox = new System.Windows.Forms.GroupBox();
            this.layoutBox = new System.Windows.Forms.GroupBox();
            this.ParamN = new System.Windows.Forms.NumericUpDown();
            this.operationBox = new System.Windows.Forms.GroupBox();
            this.OperationSelect = new System.Windows.Forms.ComboBox();
            this.functionBox = new System.Windows.Forms.GroupBox();
            this.FunctionSelect = new System.Windows.Forms.NumericUpDown();
            this.secondNumberBox = new System.Windows.Forms.GroupBox();
            this.secondShowButton = new System.Windows.Forms.Button();
            this.secondLabelD = new System.Windows.Forms.Label();
            this.SecondParamD = new System.Windows.Forms.NumericUpDown();
            this.secondLabelC = new System.Windows.Forms.Label();
            this.SecondParamC = new System.Windows.Forms.NumericUpDown();
            this.secondLabelB = new System.Windows.Forms.Label();
            this.SecondParamB = new System.Windows.Forms.NumericUpDown();
            this.secondLabelA = new System.Windows.Forms.Label();
            this.SecondParamA = new System.Windows.Forms.NumericUpDown();
            this.chart2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.firstNumberBox = new System.Windows.Forms.GroupBox();
            this.firstShowButton = new System.Windows.Forms.Button();
            this.firstLabelD = new System.Windows.Forms.Label();
            this.FirstParamD = new System.Windows.Forms.NumericUpDown();
            this.firstLabelC = new System.Windows.Forms.Label();
            this.FirstParamC = new System.Windows.Forms.NumericUpDown();
            this.firstLableB = new System.Windows.Forms.Label();
            this.FirstParamB = new System.Windows.Forms.NumericUpDown();
            this.firstLabelA = new System.Windows.Forms.Label();
            this.FirstParamA = new System.Windows.Forms.NumericUpDown();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.outputBox = new System.Windows.Forms.GroupBox();
            this.GoButton = new System.Windows.Forms.Button();
            this.chart3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.inputBox.SuspendLayout();
            this.layoutBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ParamN)).BeginInit();
            this.operationBox.SuspendLayout();
            this.functionBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FunctionSelect)).BeginInit();
            this.secondNumberBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).BeginInit();
            this.firstNumberBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.outputBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).BeginInit();
            this.SuspendLayout();
            // 
            // inputBox
            // 
            this.inputBox.Controls.Add(this.layoutBox);
            this.inputBox.Controls.Add(this.operationBox);
            this.inputBox.Controls.Add(this.functionBox);
            this.inputBox.Controls.Add(this.secondNumberBox);
            this.inputBox.Controls.Add(this.firstNumberBox);
            this.inputBox.Location = new System.Drawing.Point(12, 12);
            this.inputBox.Name = "inputBox";
            this.inputBox.Size = new System.Drawing.Size(609, 470);
            this.inputBox.TabIndex = 0;
            this.inputBox.TabStop = false;
            this.inputBox.Text = "Входные данные";
            // 
            // layoutBox
            // 
            this.layoutBox.Controls.Add(this.ParamN);
            this.layoutBox.Location = new System.Drawing.Point(267, 196);
            this.layoutBox.Name = "layoutBox";
            this.layoutBox.Size = new System.Drawing.Size(76, 46);
            this.layoutBox.TabIndex = 4;
            this.layoutBox.TabStop = false;
            this.layoutBox.Text = "Уровней";
            // 
            // ParamN
            // 
            this.ParamN.Location = new System.Drawing.Point(6, 20);
            this.ParamN.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.ParamN.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.ParamN.Name = "ParamN";
            this.ParamN.Size = new System.Drawing.Size(64, 20);
            this.ParamN.TabIndex = 0;
            this.ParamN.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // operationBox
            // 
            this.operationBox.Controls.Add(this.OperationSelect);
            this.operationBox.Location = new System.Drawing.Point(267, 129);
            this.operationBox.Name = "operationBox";
            this.operationBox.Size = new System.Drawing.Size(76, 46);
            this.operationBox.TabIndex = 3;
            this.operationBox.TabStop = false;
            this.operationBox.Text = "Операция";
            // 
            // OperationSelect
            // 
            this.OperationSelect.FormattingEnabled = true;
            this.OperationSelect.Items.AddRange(new object[] {
            "+",
            "-",
            "*",
            "/"});
            this.OperationSelect.Location = new System.Drawing.Point(6, 20);
            this.OperationSelect.Name = "OperationSelect";
            this.OperationSelect.Size = new System.Drawing.Size(64, 21);
            this.OperationSelect.TabIndex = 0;
            // 
            // functionBox
            // 
            this.functionBox.Controls.Add(this.FunctionSelect);
            this.functionBox.Location = new System.Drawing.Point(267, 65);
            this.functionBox.Name = "functionBox";
            this.functionBox.Size = new System.Drawing.Size(76, 46);
            this.functionBox.TabIndex = 2;
            this.functionBox.TabStop = false;
            this.functionBox.Text = "Функция";
            // 
            // FunctionSelect
            // 
            this.FunctionSelect.Location = new System.Drawing.Point(6, 20);
            this.FunctionSelect.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
            this.FunctionSelect.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.FunctionSelect.Name = "FunctionSelect";
            this.FunctionSelect.Size = new System.Drawing.Size(64, 20);
            this.FunctionSelect.TabIndex = 0;
            this.FunctionSelect.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // secondNumberBox
            // 
            this.secondNumberBox.Controls.Add(this.secondShowButton);
            this.secondNumberBox.Controls.Add(this.secondLabelD);
            this.secondNumberBox.Controls.Add(this.SecondParamD);
            this.secondNumberBox.Controls.Add(this.secondLabelC);
            this.secondNumberBox.Controls.Add(this.SecondParamC);
            this.secondNumberBox.Controls.Add(this.secondLabelB);
            this.secondNumberBox.Controls.Add(this.SecondParamB);
            this.secondNumberBox.Controls.Add(this.secondLabelA);
            this.secondNumberBox.Controls.Add(this.SecondParamA);
            this.secondNumberBox.Controls.Add(this.chart2);
            this.secondNumberBox.Location = new System.Drawing.Point(349, 19);
            this.secondNumberBox.Name = "secondNumberBox";
            this.secondNumberBox.Size = new System.Drawing.Size(254, 445);
            this.secondNumberBox.TabIndex = 1;
            this.secondNumberBox.TabStop = false;
            this.secondNumberBox.Text = "Число 2";
            // 
            // secondShowButton
            // 
            this.secondShowButton.Location = new System.Drawing.Point(8, 366);
            this.secondShowButton.Name = "secondShowButton";
            this.secondShowButton.Size = new System.Drawing.Size(240, 39);
            this.secondShowButton.TabIndex = 18;
            this.secondShowButton.Text = "Просмотр";
            this.secondShowButton.UseVisualStyleBackColor = true;
            this.secondShowButton.Click += new System.EventHandler(this.secondShowButton_Click);
            // 
            // secondLabelD
            // 
            this.secondLabelD.AutoSize = true;
            this.secondLabelD.Location = new System.Drawing.Point(152, 315);
            this.secondLabelD.Name = "secondLabelD";
            this.secondLabelD.Size = new System.Drawing.Size(15, 13);
            this.secondLabelD.TabIndex = 17;
            this.secondLabelD.Text = "D";
            // 
            // SecondParamD
            // 
            this.SecondParamD.DecimalPlaces = 2;
            this.SecondParamD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.SecondParamD.Location = new System.Drawing.Point(175, 313);
            this.SecondParamD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SecondParamD.Name = "SecondParamD";
            this.SecondParamD.Size = new System.Drawing.Size(64, 20);
            this.SecondParamD.TabIndex = 16;
            this.SecondParamD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // secondLabelC
            // 
            this.secondLabelC.AutoSize = true;
            this.secondLabelC.Location = new System.Drawing.Point(14, 315);
            this.secondLabelC.Name = "secondLabelC";
            this.secondLabelC.Size = new System.Drawing.Size(14, 13);
            this.secondLabelC.TabIndex = 15;
            this.secondLabelC.Text = "C";
            // 
            // SecondParamC
            // 
            this.SecondParamC.DecimalPlaces = 2;
            this.SecondParamC.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.SecondParamC.Location = new System.Drawing.Point(37, 313);
            this.SecondParamC.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SecondParamC.Name = "SecondParamC";
            this.SecondParamC.Size = new System.Drawing.Size(64, 20);
            this.SecondParamC.TabIndex = 14;
            this.SecondParamC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // secondLabelB
            // 
            this.secondLabelB.AutoSize = true;
            this.secondLabelB.Location = new System.Drawing.Point(152, 272);
            this.secondLabelB.Name = "secondLabelB";
            this.secondLabelB.Size = new System.Drawing.Size(14, 13);
            this.secondLabelB.TabIndex = 13;
            this.secondLabelB.Text = "B";
            // 
            // SecondParamB
            // 
            this.SecondParamB.DecimalPlaces = 2;
            this.SecondParamB.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.SecondParamB.Location = new System.Drawing.Point(175, 270);
            this.SecondParamB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SecondParamB.Name = "SecondParamB";
            this.SecondParamB.Size = new System.Drawing.Size(64, 20);
            this.SecondParamB.TabIndex = 12;
            this.SecondParamB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // secondLabelA
            // 
            this.secondLabelA.AutoSize = true;
            this.secondLabelA.Location = new System.Drawing.Point(14, 272);
            this.secondLabelA.Name = "secondLabelA";
            this.secondLabelA.Size = new System.Drawing.Size(14, 13);
            this.secondLabelA.TabIndex = 11;
            this.secondLabelA.Text = "A";
            // 
            // SecondParamA
            // 
            this.SecondParamA.DecimalPlaces = 2;
            this.SecondParamA.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.SecondParamA.Location = new System.Drawing.Point(37, 270);
            this.SecondParamA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.SecondParamA.Name = "SecondParamA";
            this.SecondParamA.Size = new System.Drawing.Size(64, 20);
            this.SecondParamA.TabIndex = 10;
            this.SecondParamA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chart2
            // 
            this.chart2.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chart2.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chartArea1.Name = "ChartArea1";
            this.chart2.ChartAreas.Add(chartArea1);
            legend1.Enabled = false;
            legend1.ItemColumnSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.Line;
            legend1.Name = "Legend1";
            this.chart2.Legends.Add(legend1);
            this.chart2.Location = new System.Drawing.Point(6, 19);
            this.chart2.Name = "chart2";
            this.chart2.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Число 1";
            this.chart2.Series.Add(series1);
            this.chart2.Size = new System.Drawing.Size(241, 230);
            this.chart2.TabIndex = 1;
            // 
            // firstNumberBox
            // 
            this.firstNumberBox.Controls.Add(this.firstShowButton);
            this.firstNumberBox.Controls.Add(this.firstLabelD);
            this.firstNumberBox.Controls.Add(this.FirstParamD);
            this.firstNumberBox.Controls.Add(this.firstLabelC);
            this.firstNumberBox.Controls.Add(this.FirstParamC);
            this.firstNumberBox.Controls.Add(this.firstLableB);
            this.firstNumberBox.Controls.Add(this.FirstParamB);
            this.firstNumberBox.Controls.Add(this.firstLabelA);
            this.firstNumberBox.Controls.Add(this.FirstParamA);
            this.firstNumberBox.Controls.Add(this.chart1);
            this.firstNumberBox.Location = new System.Drawing.Point(6, 19);
            this.firstNumberBox.Name = "firstNumberBox";
            this.firstNumberBox.Size = new System.Drawing.Size(254, 445);
            this.firstNumberBox.TabIndex = 0;
            this.firstNumberBox.TabStop = false;
            this.firstNumberBox.Text = "Число 1";
            // 
            // firstShowButton
            // 
            this.firstShowButton.Location = new System.Drawing.Point(0, 366);
            this.firstShowButton.Name = "firstShowButton";
            this.firstShowButton.Size = new System.Drawing.Size(248, 39);
            this.firstShowButton.TabIndex = 9;
            this.firstShowButton.Text = "Просмотр";
            this.firstShowButton.UseVisualStyleBackColor = true;
            this.firstShowButton.Click += new System.EventHandler(this.firstShowButton_Click);
            // 
            // firstLabelD
            // 
            this.firstLabelD.AutoSize = true;
            this.firstLabelD.Location = new System.Drawing.Point(144, 315);
            this.firstLabelD.Name = "firstLabelD";
            this.firstLabelD.Size = new System.Drawing.Size(15, 13);
            this.firstLabelD.TabIndex = 8;
            this.firstLabelD.Text = "D";
            // 
            // FirstParamD
            // 
            this.FirstParamD.DecimalPlaces = 2;
            this.FirstParamD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.FirstParamD.Location = new System.Drawing.Point(167, 313);
            this.FirstParamD.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.FirstParamD.Name = "FirstParamD";
            this.FirstParamD.Size = new System.Drawing.Size(64, 20);
            this.FirstParamD.TabIndex = 7;
            this.FirstParamD.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // firstLabelC
            // 
            this.firstLabelC.AutoSize = true;
            this.firstLabelC.Location = new System.Drawing.Point(6, 315);
            this.firstLabelC.Name = "firstLabelC";
            this.firstLabelC.Size = new System.Drawing.Size(14, 13);
            this.firstLabelC.TabIndex = 6;
            this.firstLabelC.Text = "C";
            // 
            // FirstParamC
            // 
            this.FirstParamC.DecimalPlaces = 2;
            this.FirstParamC.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.FirstParamC.Location = new System.Drawing.Point(29, 313);
            this.FirstParamC.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.FirstParamC.Name = "FirstParamC";
            this.FirstParamC.Size = new System.Drawing.Size(64, 20);
            this.FirstParamC.TabIndex = 5;
            this.FirstParamC.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // firstLableB
            // 
            this.firstLableB.AutoSize = true;
            this.firstLableB.Location = new System.Drawing.Point(144, 272);
            this.firstLableB.Name = "firstLableB";
            this.firstLableB.Size = new System.Drawing.Size(14, 13);
            this.firstLableB.TabIndex = 4;
            this.firstLableB.Text = "B";
            // 
            // FirstParamB
            // 
            this.FirstParamB.DecimalPlaces = 2;
            this.FirstParamB.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.FirstParamB.Location = new System.Drawing.Point(167, 270);
            this.FirstParamB.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.FirstParamB.Name = "FirstParamB";
            this.FirstParamB.Size = new System.Drawing.Size(64, 20);
            this.FirstParamB.TabIndex = 3;
            this.FirstParamB.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // firstLabelA
            // 
            this.firstLabelA.AutoSize = true;
            this.firstLabelA.Location = new System.Drawing.Point(6, 272);
            this.firstLabelA.Name = "firstLabelA";
            this.firstLabelA.Size = new System.Drawing.Size(14, 13);
            this.firstLabelA.TabIndex = 2;
            this.firstLabelA.Text = "A";
            // 
            // FirstParamA
            // 
            this.FirstParamA.DecimalPlaces = 2;
            this.FirstParamA.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.FirstParamA.Location = new System.Drawing.Point(29, 270);
            this.FirstParamA.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.FirstParamA.Name = "FirstParamA";
            this.FirstParamA.Size = new System.Drawing.Size(64, 20);
            this.FirstParamA.TabIndex = 1;
            this.FirstParamA.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // chart1
            // 
            this.chart1.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chart1.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend2.Enabled = false;
            legend2.ItemColumnSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.Line;
            legend2.Name = "Legend1";
            this.chart1.Legends.Add(legend2);
            this.chart1.Location = new System.Drawing.Point(7, 20);
            this.chart1.Name = "chart1";
            this.chart1.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Число 1";
            this.chart1.Series.Add(series2);
            this.chart1.Size = new System.Drawing.Size(241, 230);
            this.chart1.TabIndex = 0;
            // 
            // outputBox
            // 
            this.outputBox.Controls.Add(this.GoButton);
            this.outputBox.Controls.Add(this.chart3);
            this.outputBox.Location = new System.Drawing.Point(641, 12);
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(299, 470);
            this.outputBox.TabIndex = 1;
            this.outputBox.TabStop = false;
            this.outputBox.Text = "Выходные данные";
            // 
            // GoButton
            // 
            this.GoButton.Location = new System.Drawing.Point(6, 385);
            this.GoButton.Name = "GoButton";
            this.GoButton.Size = new System.Drawing.Size(287, 39);
            this.GoButton.TabIndex = 6;
            this.GoButton.Text = "Начать";
            this.GoButton.UseVisualStyleBackColor = true;
            this.GoButton.Click += new System.EventHandler(this.GoButton_Click);
            // 
            // chart3
            // 
            this.chart3.BackImageAlignment = System.Windows.Forms.DataVisualization.Charting.ChartImageAlignmentStyle.Center;
            this.chart3.BorderSkin.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.DashDotDot;
            chartArea3.Name = "ChartArea1";
            this.chart3.ChartAreas.Add(chartArea3);
            legend3.Enabled = false;
            legend3.ItemColumnSeparator = System.Windows.Forms.DataVisualization.Charting.LegendSeparatorStyle.Line;
            legend3.Name = "Legend1";
            this.chart3.Legends.Add(legend3);
            this.chart3.Location = new System.Drawing.Point(6, 19);
            this.chart3.Name = "chart3";
            this.chart3.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Число 1";
            this.chart3.Series.Add(series3);
            this.chart3.Size = new System.Drawing.Size(287, 249);
            this.chart3.TabIndex = 5;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(952, 494);
            this.Controls.Add(this.outputBox);
            this.Controls.Add(this.inputBox);
            this.Name = "Form1";
            this.Text = "Лабораторная работа №3";
            this.inputBox.ResumeLayout(false);
            this.layoutBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ParamN)).EndInit();
            this.operationBox.ResumeLayout(false);
            this.functionBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.FunctionSelect)).EndInit();
            this.secondNumberBox.ResumeLayout(false);
            this.secondNumberBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SecondParamA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart2)).EndInit();
            this.firstNumberBox.ResumeLayout(false);
            this.firstNumberBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FirstParamA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.outputBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chart3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox inputBox;
        private System.Windows.Forms.GroupBox functionBox;
        private System.Windows.Forms.NumericUpDown FunctionSelect;
        private System.Windows.Forms.GroupBox secondNumberBox;
        private System.Windows.Forms.GroupBox firstNumberBox;
        private System.Windows.Forms.GroupBox outputBox;
        private System.Windows.Forms.GroupBox layoutBox;
        private System.Windows.Forms.NumericUpDown ParamN;
        private System.Windows.Forms.GroupBox operationBox;
        private System.Windows.Forms.ComboBox OperationSelect;
        private System.Windows.Forms.Button secondShowButton;
        private System.Windows.Forms.Label secondLabelD;
        private System.Windows.Forms.NumericUpDown SecondParamD;
        private System.Windows.Forms.Label secondLabelC;
        private System.Windows.Forms.NumericUpDown SecondParamC;
        private System.Windows.Forms.Label secondLabelB;
        private System.Windows.Forms.NumericUpDown SecondParamB;
        private System.Windows.Forms.Label secondLabelA;
        private System.Windows.Forms.NumericUpDown SecondParamA;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart2;
        private System.Windows.Forms.Button firstShowButton;
        private System.Windows.Forms.Label firstLabelD;
        private System.Windows.Forms.NumericUpDown FirstParamD;
        private System.Windows.Forms.Label firstLabelC;
        private System.Windows.Forms.NumericUpDown FirstParamC;
        private System.Windows.Forms.Label firstLableB;
        private System.Windows.Forms.NumericUpDown FirstParamB;
        private System.Windows.Forms.Label firstLabelA;
        private System.Windows.Forms.NumericUpDown FirstParamA;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button GoButton;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart3;
    }
}