using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace Lab2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            OperationSelect.SelectedItem = OperationSelect.Items[0];
        }

        private void firstShowButton_Click(object sender, EventArgs e)
        {
            Double a = Double.Parse(FirstParamA.Value.ToString());
            Double b = Double.Parse(FirstParamB.Value.ToString());
            Double c = Double.Parse(FirstParamC.Value.ToString());
            Double d = Double.Parse(FirstParamD.Value.ToString());
            Int32 N = Int32.Parse(ParamN.Value.ToString());
            Int32 Func = int.Parse(FunctionSelect.Value.ToString());
            TFuzzy number = new TFuzzy(Func, N, a, b, c, d);
            number.Draw(chart1);
        }

        private void secondShowButton_Click(object sender, EventArgs e)
        {
            Double a = Double.Parse(SecondParamA.Value.ToString());
            Double b = Double.Parse(SecondParamB.Value.ToString());
            Double c = Double.Parse(SecondParamC.Value.ToString());
            Double d = Double.Parse(SecondParamD.Value.ToString());
            Int32 N = Int32.Parse(ParamN.Value.ToString());
            Int32 Func = int.Parse(FunctionSelect.Value.ToString());
            TFuzzy number = new TFuzzy(Func, N, a, b, c, d);
            number.Draw(chart2);
        }

        private void GoButton_Click(object sender, EventArgs e)
        {
            Double a1 = Double.Parse(FirstParamA.Value.ToString());
            Double b1 = Double.Parse(FirstParamB.Value.ToString());
            Double c1 = Double.Parse(FirstParamC.Value.ToString());
            Double d1 = Double.Parse(FirstParamD.Value.ToString());
            Double a2 = Double.Parse(SecondParamA.Value.ToString());
            Double b2 = Double.Parse(SecondParamB.Value.ToString());
            Double c2 = Double.Parse(SecondParamC.Value.ToString());
            Double d2 = Double.Parse(SecondParamD.Value.ToString());
            Int32 N = Int32.Parse(ParamN.Value.ToString());
            Int32 Func = int.Parse(FunctionSelect.Value.ToString());

            TFuzzy firstNumber = new TFuzzy(Func, N, a1, b1, c1, d1);
            TFuzzy secondNumber= new TFuzzy(Func, N, a2, b2, c2, d2);
            TFuzzy resultNumber = new TFuzzy();
            try
            {
                switch (OperationSelect.SelectedItem.ToString())
                {
                    case "+":
                        resultNumber = firstNumber + secondNumber;
                        break;
                    case "-":
                        resultNumber = firstNumber - secondNumber;
                        break;
                    case "*":
                        resultNumber = firstNumber * secondNumber;
                        break;
                    case "/":
                        resultNumber = firstNumber / secondNumber;
                        break;
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message,"Ошибка", MessageBoxButtons.OK);
            }
            resultNumber.Draw(chart3);
        }
    }

    public class TFuzzy
    {
        private class AlphaLayout
        {
            public double Alpha { get; set; }
            public double TopValue { get; set; }
            public double BottomValue { get; set; }

            public AlphaLayout(double alpha, double top, double bottom)
            {
                this.Alpha = alpha;
                this.TopValue = top;
                this.BottomValue = bottom;
            }

            public static double m1(double x, double a, double b)
            {
                if (x <= a) return 0;
                else if (a <= x && x <= (a + b) / 2) return (2 * Math.Pow(x - a, 2) / Math.Pow(b - a, 2));
                else if ((a + b) / 2 <= x && x <= b) return (1 - (2 * Math.Pow(x - b, 2) / Math.Pow(b - a, 2)));
                else return 1;
            }
            public static double m2(double x, double a, double b, double c)
            {
                if (a < x && x < b) return AlphaLayout.m1(x, a, b);
                else if (b <= x && x <= c) return 1;
                else if (c < x && x < c + a) return (1 - AlphaLayout.m1(x, c, c + b - a));
                else return 0;
            }
            public static double m3(double x, double a, double b, double c)
            {
                if (x <= a) return 0;
                else if (a <= x && x <= c) return ((x - a) / (c - a));
                else if (c <= x && x <= b) return ((b - x) / (b - c));
                else return 0;
            }
            public static double m4(double x, double a, double b, double c)
            {
                if (x <= c) return 1;
                else return Math.Pow(1 + Math.Pow(a * (x - c), b), -1);
            }
            public static double m5(double x, double a, double b, double c, double d)
            {
                if (x <= a) return 0;
                else if (a <= x && x <= c) return ((x - a) / (c - a));
                else if (c <= x && x <= d) return 1;
                else if (d <= x && x <= b) return ((b - x) / (b - d));
                else return 0;
            }
            public static double m6(double x, double a, double b)
            {
                if (x < a - 3 * b || x > a + 3 * b) return 0;
                else return Math.Exp(-1 * Math.Pow(x - a, 2) / (2 * Math.Pow(b, 2)));
            }
            public static double m7(double x, double a, double b)
            {
                return Math.Pow(1 + Math.Exp(-1 * a * (x - b)), -1);
            }
        }
        private class Point
        {
            public double x;
            public AlphaLayout layout;
            public Point(double x, AlphaLayout layout)
            {
                this.x = x;
                this.layout = layout;
            }
            public bool Equals(Point other)
            {
                if (Object.ReferenceEquals(other, null)) return false;
                if (Object.ReferenceEquals(this, other)) return true;
                return x.Equals(other.x);
            }
        }

        private List<AlphaLayout> A = new List<AlphaLayout>();
        private int FunctionNumber = 0;
        private double step = 1;
        private double a, b, c, d;
        public TFuzzy(int functionNum, int N, double a, double b, double c = 0, double d = 0)
        {
            try {
                this.step = Convert.ToDouble(1) / ( N - 1 );
                this.FunctionNumber = functionNum;
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
                switch (functionNum)
                {
                    case 1:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = a; x <=  b; x += step) {
                                var temp = Math.Round(AlphaLayout.m1(x, a, b), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = b + step;
                            else if (sX.Count == 1)
                            {
                                top = bottom = sX.ToArray()[0];
                            }
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count -1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 2:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = a; x <= (a + c); x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m2(x, a, b, c), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = (b + c) / 2;
                            else if (sX.Count == 1)
                            {
                                bottom = top = sX.ToArray()[0];
                            }
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 3:
                        for (double i = 0; i <= 1; i += i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = a; x <= b; x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m3(x, a, b, c), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = c;
                            else if (sX.Count == 1) bottom = top = sX.ToArray()[0];
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 4:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = 0; x <= 4 * c; x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m4(x, a, b, c), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = c - step;
                            else if (sX.Count == 1)
                                bottom = top = sX.ToArray()[0];
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 5:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = a; x <= b; x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m5(x, a, b, c, d), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = (c + d) / 2;
                            else if (sX.Count == 1)
                                bottom = top = sX.ToArray()[0];
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 6:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = a - 3 * b; x <= a + 3 * b; x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m6(x, a, b), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = a;
                            else if (sX.Count == 1)
                                bottom = top = sX.ToArray()[0];
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    case 7:
                        for (double i = 0; i <= 1; i += step)
                        {
                            double bottom = 2, top = 0, x;
                            List<double> sX = new List<double>();
                            for (x = 0; x < 2*b; x += step)
                            {
                                var temp = Math.Round(AlphaLayout.m7(x, a, b), 4);
                                if (temp >= i) sX.Add(Math.Round(x, 4));
                            }
                            if (sX.Count == 0) bottom = top = 2 * b + step;
                            else if (sX.Count == 1)
                                bottom = top = sX.ToArray()[0];
                            else
                            {
                                bottom = sX.ToArray()[0];
                                top = sX.ToArray()[sX.Count - 1];
                            }
                            AlphaLayout tmp = new AlphaLayout(i, top, bottom);
                            A.Add(tmp);
                        }
                        A.Sort((l, r) => l.Alpha.CompareTo(r.Alpha));
                        break;
                    default:
                        MessageBox.Show("Неправильная функция");
                        break;
                }
            }
            catch { MessageBox.Show("Некорректные данные", "Ошибка", MessageBoxButtons.OK); }
        }
        public TFuzzy() { }

        public void Draw(Chart place)
        {
            place.Series.Clear();
            place.Series.Add(String.Format("График {0}", FunctionNumber));
            place.Series[0].ChartType = SeriesChartType.Line;
            place.Series[0].BorderWidth = 5;
            place.Series[0].BorderWidth = 2;
            place.Series[0].Points.Clear();
            switch (this.FunctionNumber)
            {
                case 1:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A) {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m1(x, a, b),4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m1(x, a, b), 4));
                    }
                    break;
                case 2:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m2(x, a, b, c),4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m2(x, a, b, c), 4));
                    }
                    break;
                case 3:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m3(x, a, b, c), 4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m3(x, a, b, c), 4));
                    }
                    break;
                case 4:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m4(x, a, b, c), 4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m4(x, a, b, c), 4));
                    }
                    break;
                case 5:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m5(x, a, b, c, d), 4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m5(x, a, b, c, d), 4));
                    }
                    break;
                case 6:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m6(x, a, b), 4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m6(x, a, b), 4));
                    }
                    break;
                case 7:
                    A.Sort((l, r) => l.BottomValue.CompareTo(r.BottomValue));
                    foreach (var point in A)
                    {
                        var x = point.BottomValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m7(x, a, b), 4));
                    }
                    A.Sort((l, r) => l.TopValue.CompareTo(r.TopValue));
                    foreach (var point in A)
                    {
                        var x = point.TopValue;
                        place.Series[0].Points.AddXY(Math.Round(x, 4), Math.Round(AlphaLayout.m7(x, a, b), 4));
                    }
                    break;
            }
        }        

        public static TFuzzy operator+(TFuzzy a, TFuzzy b)
        {
            TFuzzy result = new TFuzzy();
            result.FunctionNumber = a.FunctionNumber;
            result.a = a.a + b.a;
            result.b = a.b + b.b;
            result.c = a.c + b.c;
            result.d = a.d + b.d;
            result.step = a.step + b.step;
            for (var i = 0; i<a.A.Count; i++)
            {
                result.A.Add(new AlphaLayout(a.A[i].Alpha, a.A[i].TopValue + b.A[i].TopValue, a.A[i].BottomValue + b.A[i].BottomValue));
            }
            return result;
        }

        public static TFuzzy operator -(TFuzzy a, TFuzzy b)
        {
            TFuzzy result = new TFuzzy();
            result.FunctionNumber = a.FunctionNumber;
            result.a = a.a - b.a;
            result.b = a.b - b.b;
            result.c = a.c - b.c;
            result.d = a.d - b.d;
            result.step = a.step - b.step;
            for (var i = 0; i < a.A.Count; i++)
            {
                result.A.Add(new AlphaLayout(a.A[i].Alpha, a.A[i].TopValue  - b.A[i].BottomValue, a.A[i].BottomValue - b.A[i].TopValue));
            }
            return result;
        }

        public static TFuzzy operator *(TFuzzy a, TFuzzy b)
        {
            TFuzzy result = new TFuzzy();
            result.FunctionNumber = a.FunctionNumber;
            result.a = a.a * b.a;
            result.b = a.b * b.b;
            result.c = a.c * b.c;
            result.d = a.d * b.d;
            result.step = a.step * b.step;
            for (var i = 0; i < a.A.Count; i++)
            {
                result.A.Add(new AlphaLayout(a.A[i].Alpha, a.A[i].TopValue * b.A[i].TopValue, a.A[i].BottomValue * b.A[i].BottomValue));
            }
            return result;
        }

        public static TFuzzy operator /(TFuzzy a, TFuzzy b)
        {
            TFuzzy result = new TFuzzy();
            result.FunctionNumber = a.FunctionNumber;
            result.a = a.a / b.a;
            result.b = a.b / b.b;
            result.c = a.c / b.c;
            result.d = a.d / b.d;
            result.step = a.step / b.step;
            for (var i = 0; i < a.A.Count; i++)
            {
                result.A.Add(new AlphaLayout(a.A[i].Alpha, a.A[i].TopValue / b.A[i].BottomValue, b.A[i].TopValue / a.A[i].BottomValue));
            }
            return result;
        }
    }
}
